<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Base Site URL
|--------------------------------------------------------------------------
|
| URL to your CodeIgniter root. Typically this will be your base URL,
| WITH a trailing slash:
|
|	http://example.com/
|
| If this is not set then CodeIgniter will guess the protocol, domain and
| path to your installation.
|
*/
$config['base_url']	= 'http://localhost/jgcaballero95/';

$config['site_title'] = 'One boring afternoon';
$config['site_description'] = 'I was very bored so I created one website from scratch. Seemed like a good idea while I was taking a shower';
$config['site_author'] = 'Jose Guillermo Caballero';
$config['site_keywords'] = 'CodeIgniter, Jose, Caballero, UTEP, University Of Texas At El Paso, Computer Science';



/* End of file custom.php */
/* Location: ./application/config/development/custom.php */