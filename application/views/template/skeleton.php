<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<title><?php echo $title ?></title>
	<meta name="description" content="<?php echo $description ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="keywords" content="<?php echo $keywords ?>" />
	<meta name="author" content="<?php echo $author ?>" />
	

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo base_url(CSS."bootstrap.min.css");?>">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="<?php echo base_url(CSS."clean-blog.min.css");?>">
   

    <!--external css-->
    <link href="<?php echo base_url(CSS."font-awesome.css");?>" rel="stylesheet" />   
        
    <!-- HTML5 Shiv and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
  <body>
	<?php echo $body ?>
	<!-- js placed at the end of the document so the pages load faster -->
        <script src="<?php echo base_url(JS."jquery.min.js");?>"></script>
        <script src="<?php echo base_url(JS."bootstrap.min.js");?>"></script>
        <script src="<?php echo base_url(JS."clean-blog.min.js");?>"></script>
        <script src="<?php echo base_url(JS."jqBootstrapValidation.js");?>"></script>
        <script src="<?php echo base_url(JS."contact_me.js");?>"></script>
       
  </body>
</html>

