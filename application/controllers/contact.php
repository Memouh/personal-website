<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contact extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}
	
    
	public function index($renderData=""){	         
		$this->title = "Jose Caballero | Contact";
		$this->keywords = "Jose Caballero, Cd. Juarez";
        $folder = 'template';
		$this->_render('front/contact',$renderData, $folder);
	}
               
        
}
