<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class About extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}
	
    
	public function index($renderData=""){	         
		$this->title = "Jose Caballero | About";
		$this->keywords = "Jose Caballero, Cd. Juarez";
        $folder = 'template';
		$this->_render('front/about',$renderData, $folder);
	}
               
        
}
